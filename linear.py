import numpy
from pylab import plot, show, grid, xlabel, ylabel
from math import sqrt
from scipy.stats import norm
import numpy as np
import sys

def fn(x):
    y = 100*x
    return y

x = numpy.empty((5,201))
for i in range(0,201):
    x[:,i] = fn(i)
np.savetxt('workload.dat',x,fmt='%.5f')
