package simulator.synch.messages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mbelkhir
 */
public class Buffer {

    private static List<Message> messages;

    static {
        messages = new ArrayList<>();
    }

    private Buffer() {
    }

    public static List<Message> getMessages() {
        return messages;
    }

    public static void push(Message msg) {
        messages.add(msg);
    }

    public static Message peek() {
        return messages.get(0);
    }

    public static Message pop() {
        Message msg = peek();
        messages.remove(0);
        return msg;
    }

    public static void shuffle() {
        Collections.shuffle(messages);
    }

    public static void pushMany(ArrayList<Message> msgs) {
        messages.addAll(msgs);
    }

    public static int getSize() {
        return messages.size();
    }

    public static String toStringBuffer() {
        String toString = "Buffer {";
        for (Message msg : messages) {
            toString = toString + msg.toString() + ", ";
        }
        toString = toString.substring(0, toString.length() - 2);
        toString = toString + "}";
        return toString;
    }

    public static boolean checkTemp(int temp) {
        for (Message message : messages) {
            if (message.getTemp() == temp) {
                return true;
            }
        }
        return false;
    }
}
