package simulator.synch.messages;

import java.util.UUID;
import org.graphstream.graph.Graph;
import simulator.ui.TopologyUI;

/**
 *
 * @author Belkhiria
 */
public class Protocol {

    public static void onReceiptOf(Node node, Message msg, Graph graph, int temp) {
        switch (msg.getType()) {
            case DELETION: {
                //System.out.println("Node" + (msg.getSrc().whichIndexInTopology() + 1)
                //      + " send a deletion message to Node" + (node.whichIndexInTopology() + 1));
//                Boolean isFromSucc = node.getSuccs().contains(msg.getSrc());
//                if (isFromSucc) {
//                    node.getSuccs().remove(msg.getSrc());
//                } else {
//                    node.getPreds().remove(msg.getSrc());
//                }
                //System.out.println("Send a deletion_ack from node"
                //+ (node.whichIndexInTopology() + 1)
                //+ " to node" + (msg.getSrc().whichIndexInTopology() + 1));
                Buffer.push(new Message(MessageType.DELETION_ACK, msg.getSrc(), node, msg.getTemp() + 1));
                break;
            }
            case DUPLICATION: {
                //System.out.println("Node" + (msg.getSrc().whichIndexInTopology() + 1)
                //+ " send a duplication message to Node" + (node.whichIndexInTopology() + 1));
                if (node.getSuccs().contains(msg.getSrc())) {
                    for (UUID newNode : msg.getAddrs()) {
                        msg.getDest().getSuccs().add(Topology.getNodeById(newNode));
                    }
                } else {
                    for (UUID newNode : msg.getAddrs()) {
                        msg.getDest().getPreds().add(Topology.getNodeById(newNode));
                    }
                }
                Buffer.push(new Message(MessageType.DUPLICATION_ACK, msg.getSrc(), node, msg.getTemp() + 1));
                break;
            }

            case DUPLICATION_ACK: {
                Simulator.incrementMessages();
                Simulator.incrementMessagesOp2(msg, "ACK");
                //System.out.println("Duplication_ack received from node"
                //      + (msg.getSrc().whichIndexInTopology() + 1)
                //    + " to node" + (node.whichIndexInTopology() + 1));
                node.setNbAckReceived(node.getNbAckReceived() + 1);
                if (node.getNbAckReceived() == node.getNbAckExprected()) {
                    node.getTableOfNewAddresses().stream().map((newSibling) -> {
                        Buffer.push(new Message(node.getSuccs(), node.getPreds(),
                                MessageType.START, Topology.getNodeById(newSibling), node, msg.getTemp() + 1));
                        return newSibling;
                    }).forEachOrdered((newSibling) -> {
                        //      System.out.println("Send a start from node"
                        //            + (node.whichIndexInTopology() + 1)
                        //          + " to node" + (Topology.getNodeById(newSibling).whichIndexInTopology() + 1));
                    });
                }
                break;
            }
            case DELETION_ACK: {
                Simulator.incrementMessages();
                Simulator.incrementMessagesOp2(msg, "ACK");
                //System.out.println("Deletion_ack received from node"
                //      + (msg.getSrc().whichIndexInTopology() + 1)
                //    + " to node" + (node.whichIndexInTopology() + 1));
                node.setNbAckReceived(node.getNbAckReceived() + 1);
                if (node.getNbAckReceived() == node.getNbAckExprected()) {
                    //System.out.println("Node" + (node.whichIndexInTopology() + 1) + " is deleted");
                    for (Node succ : node.getSuccs()) {
                        succ.getPreds().remove(node);
                    }
                    for (Node pred : node.getPreds()) {
                        pred.getSuccs().remove(node);
                    }
                    TopologyUI.deleteNodeFromUI(graph, node);
                    Topology.getNodes().set(node.whichIndexInTopology(), new Node());
                    //Topology.delete(this);  
                }
                break;
            }
            case START: {
                Simulator.incrementMessages();
                Simulator.incrementMessagesOp2(msg, "START");
                //System.out.println("Node" + (node.whichIndexInTopology() + 1) + " is activated");
                node.setIsActive(true);
                node.setSuccs(msg.getSuccs());
                node.setPreds(msg.getPreds());
                TopologyUI.addNodeToUI(graph, node);
                break;
            }
            default:
                break;
        }
    }
}
