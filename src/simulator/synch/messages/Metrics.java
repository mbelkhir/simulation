package simulator.synch.messages;

import java.text.DecimalFormat;
import simulator.conf.Configuration;

/**
 *
 * @author mbelkhir
 */
public class Metrics {

    private float[][] metrics = new float[201][24];
    private static float tupleLostPerTemp = 0;
    private static float tupleLostPerTempOp2 = 0;

    public float[][] getMetrics() {
        return metrics;
    }

    public static void updateTupleLost(float localTupleLost) {
        tupleLostPerTemp = localTupleLost + tupleLostPerTemp;
    }

    public static void updateTupleLostOp2(float localTupleLost) {
        tupleLostPerTempOp2 = localTupleLost + tupleLostPerTempOp2;
    }

    public static void resetTupleLostPerTemp() {
        tupleLostPerTemp = 0;
    }

    public static void resetTupleLostPerTempOp2() {
        tupleLostPerTempOp2 = 0;
    }

    public static float getTupleLostPerTemp() {
        return tupleLostPerTemp;
    }

    public static float getTupleLostPerTempOp2() {
        return tupleLostPerTempOp2;
    }

    public void setFirstItr() {
        metrics[0][0] = 0;
        metrics[0][1] = Topology.getSize();
        metrics[0][2] = Configuration.getTHRES_DOWN();
        metrics[0][3] = Configuration.getTHRES_TOP();
        metrics[0][4] = Configuration.getRATIO_DESIRED_LOAD();
        metrics[0][5] = 0.0f;
        metrics[0][6] = 0.0f;
        metrics[0][7] = 0.0f;
        metrics[0][8] = 0.0f;
        metrics[0][9] = SimMetrics.getLoadOperator("op1");
        metrics[0][10] = Topology.numberInstanceOp("op2");
        metrics[0][11] = SimMetrics.getLoadOperator("op2");
        metrics[0][12] = SimMetrics.getLoadOperator("op3");
        metrics[0][13] = SimMetrics.getLoadOperator("op4");
        metrics[0][14] = SimMetrics.getLoadOperator("op5");
        metrics[0][15] = SimMetrics.getGlobalLoadOperators();
        metrics[0][16] = 0f;
        metrics[0][17] = 0.0f;
        metrics[0][18] = 0.0f;
        metrics[0][19] = 0.0f;
        metrics[0][20] = 1.1f;
        metrics[0][21] = 1.1f;
        metrics[0][22] = 1.1f;
        metrics[0][23] = 1.1f;

    }

    void setStatMetrics(float itr, float nbrNodes, float nbrMessages, float lostTuples, float nbrMessagesOp2, float lostTupleOp2) {
        metrics[(int) itr][0] = itr;
        metrics[(int) itr][1] = nbrNodes;
        metrics[(int) itr][2] = Configuration.getTHRES_DOWN();
        metrics[(int) itr][3] = Configuration.getTHRES_TOP();
        metrics[(int) itr][4] = Configuration.getRATIO_DESIRED_LOAD();
        metrics[(int) itr][5] = nbrMessages;
        metrics[(int) itr][6] = lostTuples;
        metrics[(int) itr][7] = Float.valueOf(new DecimalFormat("#.##").format(((Node.getC() * metrics[(int) itr - 1][1]) / (Node.getC() * metrics[(int) itr - 1][1] + lostTuples)) * 100).replace(",", "."));
        metrics[(int) itr][8] = metrics[(int) itr - 1][1];
        metrics[(int) itr][9] = SimMetrics.getLoadOperator("op1");
        metrics[(int) itr][10] = Topology.numberInstanceOp("op2");
        metrics[(int) itr][11] = SimMetrics.getLoadOperator("op2");
        metrics[(int) itr][12] = SimMetrics.getLoadOperator("op3");
        metrics[(int) itr][13] = SimMetrics.getLoadOperator("op4");
        metrics[(int) itr][14] = SimMetrics.getLoadOperator("op5");
        metrics[(int) itr][15] = SimMetrics.getGlobalLoadOperators();
        metrics[(int) itr][16] = nbrMessagesOp2;
        metrics[(int) itr][17] = lostTupleOp2;
        metrics[(int) itr][18] = Float.valueOf(new DecimalFormat("#.##").format(((Node.getC() * metrics[(int) itr - 1][10]) / (Node.getC() * metrics[(int) itr - 1][10] + lostTupleOp2)) * 100).replace(",", "."));     
        metrics[(int) itr][19] = metrics[(int) itr - 1][10];
        metrics[(int) itr][20] = Topology.numberInstanceOp("op1");
        metrics[(int) itr][21] = Topology.numberInstanceOp("op3");
        metrics[(int) itr][22] = Topology.numberInstanceOp("op4");
        metrics[(int) itr][23] = Topology.numberInstanceOp("op5");
    }
}
