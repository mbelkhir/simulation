package simulator.synch.messages;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author mbelkhir
 */
public class WorkloadVariation {

    private static String[][] workload = new String[5][201];

    public void readWorkload() {

        try {
            File workloadFile = new File("workload.dat");
            BufferedReader br = new BufferedReader(new FileReader(workloadFile));
            String st;
            int index = 0;
            while ((st = br.readLine()) != null) {
                workload[index] = st.split(", ");
                for (int i = 0; i <= (workload[index].length - 1); i++) {
                    workload[index][i] = workload[index][i].replaceAll("[^\\w]", "");
                }
                index++;
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    public String[][] getWorkload() {
        return workload;
    }

    public Boolean verifyWorkload() {
        for (String[] rows : workload) {
            for (String values : rows) {
                if (Float.valueOf(values) < 0.0) {
                    return false;
                }
            }
        }
        return true;
    }

}
