package simulator.synch.messages;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import org.graphstream.graph.Graph;
import simulator.conf.Configuration;

/**
 *
 * @author mbelkhir
 */
public class Sim_Helper {

    private Sim_Helper() {
    }

    public static void applyPredsSuccsFromConf() {
        String[] split = Configuration.getINITIAL_NODES().split("\\),");
        int indexNode = 0;
        int PredOrSucc = 0;
        for (String string : split) {
            String ss = string.replaceAll("[\\(|\\)]", "");
            for (int j = 0; j < ss.length(); j++) {
                if (ss.charAt(j) == ']') {
                    PredOrSucc++;
                    if (PredOrSucc == 2) {
                        PredOrSucc = 0;
                        indexNode++;
                    }
                }
                if (Character.isDigit(ss.charAt(j))) {
                    if (PredOrSucc == 0) {
                        //inserts succs for node i
                        Topology.getNodeByIndex(indexNode).getSuccs()
                                .add(Topology.getNodeByIndex(Integer.parseInt(ss.charAt(j) + "") - 1));
                    } else if (PredOrSucc == 1) {
                        //inserts preds for node i
                        Topology.getNodeByIndex(indexNode).getPreds()
                                .add(Topology.getNodeByIndex(Integer.parseInt(ss.charAt(j) + "") - 1));
                    }
                }
            }
        }
    }

    public static void verifySuccsAndPreds(Graph graph) {
        for (simulator.synch.messages.Node node : Topology.getNodes()) {
            if (node.isIsActive()) {
                for (simulator.synch.messages.Node succ : node.getSuccs()) {
                    if (!succ.getPreds().contains(node) && succ.isIsActive()) {
                        System.out.println("ERROR IN SYNCHRONIZATION NODE " + (node.whichIndexInTopology() + 1) + "AND HIS SUCC " + (succ.whichIndexInTopology() + 1));
                        System.exit(0);
                    } else if (succ.isIsActive()) {
                        String id = "from" + (node.whichIndexInTopology() + 1)
                                + "to" + (succ.whichIndexInTopology() + 1) + "";
                        try {
                            graph.getEdge(id).addAttribute("ui.style", "fill-color: rgb(0,100,255);");
                        } catch (Exception e) {
                            System.err.println(id);
                        }
                    }
                }
                for (simulator.synch.messages.Node pred : node.getPreds()) {
                    if (!pred.getSuccs().contains(node) && pred.isIsActive()) {
                        System.out.println("ERROR IN SYNCHRONIZATION NODE " + (node.whichIndexInTopology() + 1) + "AND HIS PRED " + (pred.whichIndexInTopology() + 1));
                        System.exit(0);
                    } else if (pred.isIsActive()) {
                        String id = "from" + (pred.whichIndexInTopology() + 1)
                                + "to" + (node.whichIndexInTopology() + 1) + "";
                        try {
                            graph.getEdge(id).addAttribute("ui.style", "fill-color: rgb(0,100,255);");
                        } catch (Exception e) {
                            System.err.println(id);
                        }
                    }
                }
            }
        }
    }

    public static void toPrintTopology() {
        Topology.getNodes().stream().map((node) -> {
            System.out.print("(Node" + (node.whichIndexInTopology() + 1) + ": ");
            System.out.print("succ: ");
            return node;
        }).map((Node node) -> {
            return node;
        }).map((Node node) -> {
            node.getSuccs().forEach((succ) -> {
                System.out.print((Topology.getNodeById(succ.getId()).whichIndexInTopology() + 1) + " ");
            });
            return node;
        }).map((node) -> {
            System.out.print("preds: ");
            return node;
        }).map((node) -> {
            node.getPreds().forEach((preds) -> {
                System.out.print((Topology.getNodeById(preds.getId()).whichIndexInTopology() + 1) + " ");
            });
            return node;
        }).forEachOrdered((_item) -> {
            System.out.print(")");
        });
        System.out.println("");
    }

    public static void printMetrics(Metrics statisticMetrics) {
        for (int i = 1; i < statisticMetrics.getMetrics().length; i++) {
            for (int j = 0; j < statisticMetrics.getMetrics()[i].length; j++) {
                if (j != 1 && j != 8) {
                    System.out.print(statisticMetrics.getMetrics()[i][j] + " ");
                } else if (j != 8) {
                    System.out.print(statisticMetrics.getMetrics()[i][8] + " ");
                }
            }
            System.out.println("");
        }
    }

    public static void printMetricsToFile(Metrics statisticMetrics) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("metrics.dat"));
            for (int i = 1; i < statisticMetrics.getMetrics().length; i++) {
                for (int j = 0; j < 20; j++) {
                    if (j != 1 && j != 8 && j != 2 && j != 3 && j != 4 && j != 9 && j != 12 && j != 13 && j != 14) {
                        writer.write((int) statisticMetrics.getMetrics()[i][j] + "\t");
                    } else if (j != 8 && j != 2 && j != 3 && j != 4 && j != 9 && j != 12 && j != 13 && j != 14) {
                        writer.write((int) statisticMetrics.getMetrics()[i][8] + "\t");
                    }
                }
                writer.newLine();
            }
            writer.close();
        } catch (Exception e) {
            System.out.println("Error to write on metrics.dat ");
        }
    }
}
