package simulator.synch.messages;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author mbelkhir
 */
public class LocalScalingManager {

    private Node node;

    public LocalScalingManager(Node node) {
        this.node = node;
    }

    private float getProbability(float desiredLoad) {
//        System.out.println("getProba of Node" + (node.whichIndexInTopology() + 1));
        float globalLoad = node.getCurrentLoad() * node.getNumberCurrentInstances();
        //      System.out.println("globalLoard= " + globalLoad);
        float idealLoad = Node.getC() * desiredLoad;
        //    System.out.println("idealLoad= " + idealLoad);
        float nbrInstancesDiff = Math.abs(((float) globalLoad / idealLoad) - node.getNumberCurrentInstances());
        //  System.out.println("nbrInstancesDiff= " + nbrInstancesDiff);
        //System.out.println("proba= " + Math.abs(nbrInstancesDiff / (float) node.getNumberCurrentInstances()));
        return Math.abs(nbrInstancesDiff / (float) node.getNumberCurrentInstances());
    }

    private double getDg() {
        float globalLoad = node.getCurrentLoad() * node.getNumberCurrentInstances();
        float idealLoad = Node.getC();
        float nbrInstancesDiff = Math.abs(((float) globalLoad / idealLoad) - node.getNumberCurrentInstances());
        return Math.ceil(Math.abs(nbrInstancesDiff / (float) node.getNumberCurrentInstances()));
    }

    private double getRg() {
        return node.getCurrentLoad() / (Node.getC() * (getDg() + 1));
    }

    private int applyProba(float p) {
        return Math.random() < p ? 1 : 0;
    }

    private void scalingOut(int temp) {
        float p;
        node.resetTableOfNewAddrs();
        if (node.getCurrentLoad() / Node.getC() >= 1) {
            if (node.getFuncToDo().equals("op2")) {
                System.out.println("iteration = " + temp + " rg= " + getRg() + " l= " + node.getCurrentLoad()
                        + " n= " + node.getNumberCurrentInstances() + " C= " + Node.getC()
                        + " p= " + getProbability((float) 0.7) + " should have with the option desired load n(t+1) = "
                        + Math.abs(((node.getCurrentLoad() * node.getNumberCurrentInstances()) / (Node.getC() * Node.getRATIO_DESIRED_LOAD())))
                        + " should have without desired load n(t+1)= "
                        + Math.abs(((node.getCurrentLoad() * node.getNumberCurrentInstances()) / (Node.getC()))));
            }
            //p = getProbability((float) getRg());
            p = getProbability(Node.getRATIO_DESIRED_LOAD());
        } else {
            p = getProbability(Node.getRATIO_DESIRED_LOAD());
        }

        node.setNbAckReceived(0);
        node.setNbAckExprected(node.getSuccs().size() + node.getPreds().size());
        int nbrsNewNodes = (int) p + applyProba(p - (int) p);
        //int nbrsNewNodes = (int) pg + applyProba((float) (pg - (int) pg));
        //System.out.println("nbrsNewNodes= " + nbrsNewNodes);
        if (nbrsNewNodes != 0) {
            for (int i = 0; i < nbrsNewNodes; i++) {
                Node newNode = new Node(node.getFuncToDo());
                Topology.insert(newNode);
                node.getTableOfNewAddresses().add(newNode.getId());
            }
            for (Node succ : node.getSuccs()) {
                Simulator.incrementMessages();
                Simulator.incrementMessagesOp2(node);
                Buffer.push(new Message(node.getTableOfNewAddresses(), MessageType.DUPLICATION, succ, node, temp));
            }
            for (Node pred : node.getPreds()) {
                Simulator.incrementMessages();
                Simulator.incrementMessagesOp2(node);
                Buffer.push(new Message(node.getTableOfNewAddresses(), MessageType.DUPLICATION, pred, node, temp));
            }
        }
    }

    private void scalingIn(int temp) {
        node.setNbAckReceived(0);
        node.setNbAckExprected(node.getSuccs().size() + node.getPreds().size());
        List<UUID> thisAddr = new ArrayList<>();
        thisAddr.add(node.getId());
        if (applyProba(getProbability(Node.getRATIO_DESIRED_LOAD())) == 1) {
            node.getSuccs().forEach((succ) -> {
                Simulator.incrementMessages();
                Simulator.incrementMessagesOp2(node);
                Buffer.push(new Message(thisAddr, MessageType.DELETION, succ, node, temp));
            });
            node.getPreds().forEach((pred) -> {
                Simulator.incrementMessages();
                Simulator.incrementMessagesOp2(node);
                Buffer.push(new Message(thisAddr, MessageType.DELETION, pred, node, temp));
            });
        }
    }

    public void autoScaling(int tempo, int temp) {
        node.updateNbrCurrentInstances();
        node.updateCurrentLoad();
        //node.resetTableOfNewAddrs();
        if (Node.getC() < node.getCurrentLoad()) {
            Metrics.updateTupleLost(node.getCurrentLoad() - Node.getC());
            if (node.getFuncToDo().equals("op2")) {
                Metrics.updateTupleLostOp2(node.getCurrentLoad() - Node.getC());
            }
        }
        if ((node.getCurrentLoad() / Node.getC() >= Node.getTHRES_TOP()) && (tempo % 5 == 0)) {
            scalingOut(temp);
        } else if ((node.getCurrentLoad() / Node.getC() <= Node.getTHRES_DOWN() && !node.getisLeader()) && (tempo % 5 == 0)) {
            if (!node.getisLeader()) {
                scalingIn(temp);
            }
        }
    }
}
