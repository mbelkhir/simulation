package simulator.synch.messages;

import java.io.IOException;

/**
 *
 * @author mbelkhir
 */
public class SimulatorSynchMessages {

    public static void main(String[] args) throws InterruptedException {
        WorkloadVariation wl = new WorkloadVariation();
        Simulator simulator = new Simulator();
        // run the simulator with a conf file
        if (args.length < 1) {
            try {
                while (true) {
                    Process p = Runtime.getRuntime().exec("python3 BrownianMotion2.py");
                    //Process p = Runtime.getRuntime().exec("python3 linear.py");
                    p.waitFor();
                    wl.readWorkload();
                    if (wl.verifyWorkload()) {
                        break;
                    }
                }

            } catch (IOException e) {
                System.err.println(e);
            }
            simulator.run();

        } // run the simulator without a conf file
        else {
        }
    }
}
