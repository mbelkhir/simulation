package simulator.synch.messages;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author mbelkhir
 */
public class Topology {

    private static List<Node> nodes;

    static {
        Topology.nodes = new ArrayList<>();
    }

    private Topology() {
    }

    public static List<Node> getNodes() {
        return nodes;
    }

    public static void insert(Node node) {
        nodes.add(node);
    }

    public static void delete(Node node) {
        if (nodes.remove(node)) {
        } else {
            System.out.println("Deletion node failed: "
                    + node.toString() + " dosn't exist in the Topology");
        }
    }

    public static int getSize() {
        return nodes.size();
    }

    public static int getSizeActive() {
        int size = 0;
        for (Node node : nodes) {
            if (node.isIsActive()) {
                size++;
            }
        }
        return size;
    }

    public static void deleteHead() {
        if (!nodes.isEmpty()) {
            nodes.remove(0);
        } else {
            System.out.println("Deletion node failed: Topology is empty!!");
        }
    }

    public static Node getNodeById(UUID id) {
        for (Node node : nodes) {
            if (node.getId().equals(id)) {
                return node;
            }
        }
        System.err.println("Node with id " + id + " doesn't exist in the Topology");
        System.exit(0);
        return null;
    }

    public static Node getNodeByIndex(int i) {
        return nodes.get(i);
    }

    public static String toStringTopology() {
        String toString = "Topology {";
        for (Node node : nodes) {
            toString = toString + node.toString() + ", ";
        }
        toString = toString.substring(0, toString.length() - 2);
        toString = toString + "}";
        return toString;
    }

    public static float numberInstanceOp1() {
        float count = 0f;
        for (Node node : nodes) {
            if (node.isIsActive() && node.getFuncToDo().equals("op1")) {
                count = count + 1;
            }
        }
        return count;
    }

    public static float numberInstanceOp2() {
        float count = 0f;
        for (Node node : nodes) {
            if (node.isIsActive() && node.getFuncToDo().equals("op2")) {
                count = count + 1;
            }
        }
        return count;
    }

    public static float numberInstanceOp(String op) {
        float count = 0f;
        for (Node node : nodes) {
            if (node.isIsActive() && node.getFuncToDo().equals(op)) {
                count = count + 1;
            }
        }
        return count;
    }

}
