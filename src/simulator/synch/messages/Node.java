package simulator.synch.messages;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.graphstream.graph.Graph;
import simulator.conf.Configuration;

/**
 *
 * @author mbelkhir
 */
public class Node {

    private final UUID id;
    private static final int C;
    private String funcToDo;
    private float currentLoad;
    private int initialLoad;
    private int numberCurrentInstances;
    private int numberPastInstances;
    private static final float RATIO_DESIRED_LOAD;
    private static final float THRES_TOP;
    private static final float THRES_DOWN;
    private List<Node> succs;
    private List<Node> preds;
    private int nbAckReceived;
    private int nbAckExprected;
    private List<UUID> tableOfNewAddresses;
    private final LocalScalingManager scale;
    private boolean isLeader;
    private boolean isActive;
    private int periodScale;

    static {
        C = Configuration.getCAPACITY();
        RATIO_DESIRED_LOAD = Configuration.getRATIO_DESIRED_LOAD();
        THRES_TOP = Configuration.getTHRES_TOP();
        THRES_DOWN = Configuration.getTHRES_DOWN();
    }

    public Node(String funcToDo) {
        this.id = java.util.UUID.randomUUID();
        succs = new ArrayList<>();
        preds = new ArrayList<>();
        nbAckReceived = 0;
        isActive = false;
        scale = new LocalScalingManager(this);
        isLeader = false;
        this.funcToDo = funcToDo;
        tableOfNewAddresses = new ArrayList<>();
        periodScale = (int)(Math.random() * 3);
    }

    public Node() {
        tableOfNewAddresses = new ArrayList<>();
        this.id = java.util.UUID.randomUUID();
        succs = new ArrayList<>();
        preds = new ArrayList<>();
        nbAckReceived = 0;
        isActive = false;
        scale = new LocalScalingManager(this);
        isLeader = false;
        funcToDo = "";
        periodScale = (int)(Math.random() * 5);
    }

    public boolean isIsLeader() {
        return isLeader;
    }

    public int getPeriodScale() {
        return periodScale;
    }

    public void incrementPeriodScale() {
        this.periodScale++;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public void setFuncToDo(String funcToDo) {
        this.funcToDo = funcToDo;
    }

    public void setCurrentLoad(float currentLoad) {
        this.currentLoad = currentLoad;
    }

    public void setNumberCurrentInstances(int numberCurrentInstances) {
        this.numberCurrentInstances = numberCurrentInstances;
    }

    public boolean getisLeader() {
        return isLeader;
    }

    public void setIsLeader(boolean isLeader) {
        this.isLeader = isLeader;
    }

    public void setNbAckReceived(int nbAckReceived) {
        this.nbAckReceived = nbAckReceived;
    }

    public void setSuccs(List<Node> succs) {
        this.succs = new ArrayList<>(succs);
    }

    public void setPreds(List<Node> preds) {
        this.preds = new ArrayList<>(preds);
    }

    public Boolean isInTopology() {
        return Topology.getNodes().contains(this);
    }

    public int getNbAckReceived() {
        return nbAckReceived;
    }

    public int getNbAckExprected() {
        return nbAckExprected;
    }

    public List<UUID> getTableOfNewAddresses() {
        return tableOfNewAddresses;
    }

    public UUID getId() {
        return id;
    }

    public static int getC() {
        return C;
    }

    public String getFuncToDo() {
        return funcToDo;
    }

    public float getCurrentLoad() {
        return currentLoad;
    }

    public int getInitialLoad() {
        return initialLoad;
    }

    public int getNumberCurrentInstances() {
        return numberCurrentInstances;
    }

    public int getNumberPastInstances() {
        return numberPastInstances;
    }

    public static float getRATIO_DESIRED_LOAD() {
        return RATIO_DESIRED_LOAD;
    }

    public static float getTHRES_TOP() {
        return THRES_TOP;
    }

    public static float getTHRES_DOWN() {
        return THRES_DOWN;
    }

    public List<Node> getSuccs() {
        return succs;
    }

    public List<Node> getPreds() {
        return preds;
    }

    public void setNbAckExprected(int nbAckExprected) {
        this.nbAckExprected = nbAckExprected;
    }

    @Override
    public String toString() {
        return "Node{" + "C=" + C + ", RATIO_DESIRED_LOAD="
                + RATIO_DESIRED_LOAD + ", THRES_TOP=" + THRES_TOP
                + ", THRES_DOWN=" + THRES_DOWN + '}';
    }

    public void onReceiptOf(Message msg, Graph graph, int temp) {
        Protocol.onReceiptOf(this, msg, graph, temp);
    }

    public void resetTableOfNewAddrs() {
        tableOfNewAddresses = new ArrayList<>();
    }

    public int whichIndexInTopology() {
        return Topology.getNodes().indexOf(this);
    }

    public void doScaling(int tempo, int temp) {
        scale.autoScaling(tempo, temp);
    }

    public void updateNbrCurrentInstances() {
        int currentInstances = 0;
        currentInstances = Topology.getNodes().stream()
                .filter((node) -> (node.getFuncToDo().equals(funcToDo)))
                .filter((node) -> (node.isActive))
                .map((_item) -> 1).reduce(currentInstances, Integer::sum);
        this.numberCurrentInstances = currentInstances;
    }

    public void updateCurrentLoad() {
        this.currentLoad = SimMetrics.getLoadOperator(this.funcToDo) / this.numberCurrentInstances;
    }
}
