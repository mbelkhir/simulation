package simulator.synch.messages;

import java.util.List;
import java.util.UUID;

/**
 *
 * @author mbelkhir
 */
public class Message {

    private List<UUID> addrs;
    private List<Node> succs;
    private List<Node> preds;
    private MessageType type;
    private Node dest;
    private Node src;
    private int temp;

    @Override
    public String toString() {
        return "Message{" + " from node " + Topology.getNodeById(src.getId()).whichIndexInTopology() + " to node "
                + Topology.getNodeById(dest.getId()).whichIndexInTopology() + " type " + type + " temp " + temp + '}';
    }

    public Message(List<UUID> addrs, MessageType type, Node dest, Node src, int temp) {
        this.addrs = addrs;
        this.type = type;
        this.dest = dest;
        this.src = src;
        this.temp = temp;
    }

    public Message(List<Node> succs, List<Node> preds, MessageType type, Node dest, Node src, int temp) {
        this.succs = succs;
        this.preds = preds;
        this.type = type;
        this.dest = dest;
        this.src = src;
        this.temp = temp;
    }

    public Message(MessageType type, Node dest, Node src, int temp) {
        this.type = type;
        this.dest = dest;
        this.src = src;
        this.temp = temp;
    }

    public List<UUID> getAddrs() {
        return addrs;
    }

    public List<Node> getSuccs() {
        return succs;
    }

    public List<Node> getPreds() {
        return preds;
    }

    public MessageType getType() {
        return type;
    }

    public Node getDest() {
        return dest;
    }

    public Node getSrc() {
        return src;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }
}
