package simulator.synch.messages;

/**
 *
 * @author Belkhiria
 */
public enum MessageType {
    DUPLICATION_ACK,
    DELETION_ACK,
    DELETION,
    DUPLICATION,
    START;
}

