package simulator.synch.messages;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.MultiGraph;
import simulator.conf.Configuration;
import simulator.ui.TopologyUI;

/**
 *
 * @author mbelkhir
 */
public class Simulator {

    TopologyUI toppologyUI = new TopologyUI();
    private static int nbMessages;
    private static int nbMessagesOp2;
    Metrics statisticMetrics = new Metrics();
    SimMetrics workload = new SimMetrics();
    Graph graph = new MultiGraph("autoScaling");

    private void regulation() {
        int capacity = 500;
        int initialLoad = 5000;
        int numberNodes = (int) (initialLoad / (capacity * 0.7));
        for (int indexNode = 0; indexNode < 5; indexNode++) {
            Node node = Topology.getNodeByIndex(indexNode);
            for (int nbNode = 0; nbNode < numberNodes - 1; nbNode++) {
                Node newNode = new Node(node.getFuncToDo());
                newNode.setIsLeader(false);
                newNode.setIsActive(true);
                for (Node succ : node.getSuccs()) {
                    succ.getPreds().add(newNode);
                }
                for (Node pred : node.getPreds()) {
                    pred.getSuccs().add(newNode);
                }
                newNode.setPreds(node.getPreds());
                newNode.setSuccs(node.getSuccs());
                Topology.insert(newNode);
                TopologyUI.addNodeToUI(graph, newNode);

            }
        }
        Sim_Helper.toPrintTopology();
    }

    private void init() {
        for (int i = 0; i < Configuration.getNUMBER_INITAL_NODES(); i++) {
            Topology.insert(new Node());
            Topology.getNodeByIndex(i).setIsActive(true);
            Topology.getNodeByIndex(i).setIsLeader(true);
        }
        SimMetrics.initLoadByOperator();
        SimMetrics.getLoadByOperators().entrySet().stream().map((op) -> {
            int index = Integer.parseInt(op.getKey().replaceAll("[a-z]", "")) - 1;
            Topology.getNodeByIndex(index).setFuncToDo(op.getKey());
            return index;
        }).forEachOrdered((index) -> {
            Topology.getNodeByIndex(index).setNumberCurrentInstances(1);
        });
        System.out.println(Configuration.getINITIAL_NODES());
        Sim_Helper.applyPredsSuccsFromConf();
        //Sim_Helper.toPrintTopology();
        toppologyUI.initUI(graph);
        regulation();
        statisticMetrics.setFirstItr();
    }

    public void run() {
        init();
        int z = 0;
        while (z < 200) {
            //System.out.println("########Iteration number: " + (z + 1) + " ########");
            for (int index = 0; index < Topology.getSize(); index++) {
                if (Topology.getNodeByIndex(index).isIsActive()) {
                    Topology.getNodeByIndex(index).incrementPeriodScale();
                    Topology.getNodeByIndex(index).doScaling(Topology.getNodeByIndex(index).getPeriodScale(), z);
                }
            }
            while (!Buffer.getMessages().isEmpty()) {
                Message msg = Buffer.peek();
                if (msg.getTemp() == z) {
                    msg.getDest().onReceiptOf(msg, graph, z);
                    Buffer.getMessages().remove(msg);
                } else {
                    if (!Buffer.checkTemp(z)) {
                        break;
                    }
                }
            }
            Buffer.shuffle();
            //Sim_Helper.toPrintTopology();
            statisticMetrics.setStatMetrics(z + 1, Topology.getSizeActive(), this.nbMessages, Metrics.getTupleLostPerTemp(), this.nbMessagesOp2, Metrics.getTupleLostPerTempOp2());
            this.nbMessages = 0;
            this.nbMessagesOp2 = 0;

            Metrics.resetTupleLostPerTemp();
            Metrics.resetTupleLostPerTempOp2();
            try {
                Thread.sleep(1);
            } catch (InterruptedException ex) {
            }
            z++;
            Sim_Helper.verifySuccsAndPreds(graph);
            workload.workload(z);
        }
        Sim_Helper.printMetrics(statisticMetrics);
        Sim_Helper.printMetricsToFile(statisticMetrics);
        try {
            Process p = Runtime.getRuntime().exec("python3 plot-results.py");
            Process pGlobal = Runtime.getRuntime().exec("python3 plot-results-global.py");
            //Process pNetwork = Runtime.getRuntime().exec("python3 plot-linear-trafic.py");
//            Thread.sleep(1000);
//            p.destroy();
        } catch (IOException e) {
            System.err.println(e);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
//        }
        }
    }

    public static void incrementMessages() {
        nbMessages++;
    }

    public static void incrementMessagesOp2(Node node) {
        if (node.getFuncToDo().equals("op2")) {
            nbMessagesOp2++;
        }
    }

    public static void incrementMessagesOp2(Message msg, String stat) {
        if (stat.equals("ACK")) {
            if (msg.getDest().getFuncToDo().equals("op2")) {
                nbMessagesOp2++;
            }
        } else if (stat.equals("START")) {
            if (msg.getSrc().getFuncToDo().equals("op2")) {
                nbMessagesOp2++;
            }
        }
    }
}
