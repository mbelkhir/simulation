package simulator.synch.messages;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mbelkhir
 */
public class SimMetrics {

    private static WorkloadVariation newWorkload = new WorkloadVariation();

    private static HashMap<String, Float> loadByOperators = new HashMap<>();

    public static void initLoadByOperator() {
        loadByOperators.put("op1", Float.valueOf(newWorkload.getWorkload()[0][0]));
        loadByOperators.put("op2", Float.valueOf(newWorkload.getWorkload()[1][0]));
        loadByOperators.put("op3", Float.valueOf(newWorkload.getWorkload()[2][0]));
        loadByOperators.put("op4", Float.valueOf(newWorkload.getWorkload()[3][0]));
        loadByOperators.put("op5", Float.valueOf(newWorkload.getWorkload()[4][0]));
    }

    public static HashMap<String, Float> getLoadByOperators() {
        return loadByOperators;
    }

    public static void addOperatorLoad(String op, float load) {
        loadByOperators.put(op, load);
    }

    public static float getLoadOperator(String op) {
        return loadByOperators.get(op);
    }

    public void workload(int iteration) {
        loadByOperators.replace("op1", Float.valueOf(newWorkload.getWorkload()[0][iteration]));
        loadByOperators.replace("op2", Float.valueOf(newWorkload.getWorkload()[1][iteration]));
        loadByOperators.replace("op3", Float.valueOf(newWorkload.getWorkload()[2][iteration]));
        loadByOperators.replace("op4", Float.valueOf(newWorkload.getWorkload()[3][iteration]));
        loadByOperators.replace("op5", Float.valueOf(newWorkload.getWorkload()[4][iteration]));
    }

    public static float getGlobalLoadOperators() {
        float globalLoad = 0;
        for (Map.Entry<String, Float> loadByOperator : loadByOperators.entrySet()) {
            globalLoad = globalLoad + loadByOperator.getValue();
        }
        return globalLoad;
    }
}
