package simulator.conf;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

/**
 *
 * @author mbelkhir
 */
public class Configuration {

    private static final Integer CAPACITY;
    private static final Float RATIO_DESIRED_LOAD;
    private static final Float THRES_TOP;
    private static final Float THRES_DOWN;
    private static final Integer NUMBER_INITAL_NODES;
    private static final String INITIAL_NODES;
    private static final HashMap<String, String> aux;

    private static final Properties prop;

    static {
        prop = new Properties();
        aux = getConf();
        CAPACITY = Integer.parseInt(aux.get("c"));
        RATIO_DESIRED_LOAD = Float.parseFloat(aux.get("rdl"));
        THRES_TOP = Float.parseFloat(aux.get("thresT"));
        THRES_DOWN = Float.parseFloat(aux.get("thresD"));
        NUMBER_INITAL_NODES = Integer.parseInt(aux.get("numNodes"));
        INITIAL_NODES = aux.get("initNodes");
    }

    private Configuration() {
    }

    private static HashMap<String, String> getConf() {
        HashMap<String, String> getConf = new HashMap<>();
        try {
            FileInputStream in = new FileInputStream("configuration.properties");
            prop.load(in);
            getConf.put("c", prop.getProperty("CAPACITY"));
            getConf.put("rdl", prop.getProperty("RATIO_DESIRED_LOAD"));
            getConf.put("thresT", prop.getProperty("THRES_TOP"));
            getConf.put("thresD", prop.getProperty("THRES_DOWN"));
            getConf.put("numNodes", prop.getProperty("NUMBER_INITAL_NODES"));
            getConf.put("initNodes", prop.getProperty("INITIAL_NODES"));
            in.close();
        } catch (IOException ex) {
            getConf.put("c", "0");
            getConf.put("rdl", "0");
            getConf.put("thresT", "0");
            getConf.put("thresD", "0");
            getConf.put("numNodes", "4");
            getConf.put("initNodes", "([2,3],[]),([4],[1]),([4],[1]),([],[2,3])");
        }
        return getConf;
    }

    public static Integer getNUMBER_INITAL_NODES() {
        return NUMBER_INITAL_NODES;
    }

    public static Integer getCAPACITY() {
        return CAPACITY;
    }

    public static Float getRATIO_DESIRED_LOAD() {
        return RATIO_DESIRED_LOAD;
    }

    public static Float getTHRES_TOP() {
        return THRES_TOP;
    }

    public static Float getTHRES_DOWN() {
        return THRES_DOWN;
    }

    public static String getINITIAL_NODES() {
        return INITIAL_NODES;
    }

    public static String toStringConf() {
        return "Configuration{" + "CAPACITY=" + CAPACITY + ", RATIO_DESIRED_LOAD="
                + RATIO_DESIRED_LOAD + ", THRES_TOP=" + THRES_TOP
                + ", THRES_DOWN=" + THRES_DOWN + '}';
    }

}
