package simulator.ui;

import org.graphstream.graph.Graph;
import simulator.synch.messages.Node;
import simulator.synch.messages.Topology;

/**
 *
 * @author mbelkhir
 */
public class TopologyUI {

    private final String styleSheet
            = "node {"
            + "fill-color: red;"
            + "size: 20px;"
            + "}";

    public static void addNodeToUI(Graph graph, Node node) {
        graph.addNode((node.whichIndexInTopology() + 1) + "");
        graph.getNode((node.whichIndexInTopology() + 1) + "").addAttribute("ui.label", "Node: " + (node.whichIndexInTopology() + 1));
        for (simulator.synch.messages.Node succ : node.getSuccs()) {
            if (succ.isIsActive()) {
                String id = "from" + (node.whichIndexInTopology() + 1) + "to" + (succ.whichIndexInTopology() + 1) + "";
                graph.addEdge(id, (node.whichIndexInTopology() + 1) + "", (succ.whichIndexInTopology() + 1) + "");
            }
        }
        for (simulator.synch.messages.Node pred : node.getPreds()) {
            if (pred.isIsActive()) {
                String id = "from" + (pred.whichIndexInTopology() + 1) + "to" + (node.whichIndexInTopology() + 1) + "";
                graph.addEdge(id, (pred.whichIndexInTopology() + 1) + "", (node.whichIndexInTopology() + 1) + "");
            }
        }
    }

    public void initUI(Graph graph) {
        graph.addAttribute("ui.stylesheet", styleSheet);
        for (simulator.synch.messages.Node node : Topology.getNodes()) {
            graph.addNode((node.whichIndexInTopology() + 1) + "");
            graph.getNode((node.whichIndexInTopology() + 1) + "").addAttribute("ui.label", "Node: " + (node.whichIndexInTopology() + 1));
        }
        for (simulator.synch.messages.Node node : Topology.getNodes()) {
            for (simulator.synch.messages.Node succ : node.getSuccs()) {
                String id = "from" + (node.whichIndexInTopology() + 1) + "to" + (succ.whichIndexInTopology() + 1) + "";
                graph.addEdge(id, (node.whichIndexInTopology() + 1) + "", (succ.whichIndexInTopology() + 1) + "");
            }
        }
        //graph.display();
    }

    public static void deleteNodeFromUI(Graph graph, Node node) {
        for (Node succ : node.getSuccs()) {
            graph.getEdge("from" + (node.whichIndexInTopology() + 1) + "to" + (succ.whichIndexInTopology() + 1) + "");
        }
        for (Node pred : node.getPreds()) {
            graph.getEdge("from" + (pred.whichIndexInTopology() + 1) + "to" + (node.whichIndexInTopology() + 1) + "");
        }
        graph.removeNode((node.whichIndexInTopology() + 1) + "");
    }
}
