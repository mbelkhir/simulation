#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import math

colors=['b', 'g', 'r', 'c', 'm', 'y', 'k']
patterns=['','--','-.',':']

### set experience parameters

# in results-simple.dat, format is
# l,w,t

### open and clean data file
f1 = open("/home/mbelkhir/NetBeansProjects/simulator-synch-messages/metrics.dat")

data1 = f1.read()
data1 = data1.split('\n')

del data1[len(data1)-1]
sub_data = [row for row in data1]

### Simple - CMD ###
# plot one curve per possible length
xs = [sub_data_line.split('\t')[0] for sub_data_line in sub_data]
noeuds = [sub_data_line.split('\t')[1] for sub_data_line in sub_data]
msg = [sub_data_line.split('\t')[2] for sub_data_line in sub_data]
globalLoad = [sub_data_line.split('\t')[7] for sub_data_line in sub_data]
throughput = [sub_data_line.split('\t')[4] for sub_data_line in sub_data]
lostTuple = [sub_data_line.split('\t')[3] for sub_data_line in sub_data]
overShootAux = [float(x) - (float(y)/500) for x, y in zip(noeuds, globalLoad)]
overShootParAux = [math.ceil(float(x) - (float(y)/(500*0.7))) for x, y in zip(noeuds, globalLoad)]
overShoot = [x if x >= 0 else 0 for x in overShootAux]
overShootPar = [x if x >= 0 else 0 for x in overShootParAux]

NodesV = []
NodesV.append(int(0))
for i in range(1, len(noeuds)):
    NodesV.append(int(noeuds[i])-int(noeuds[i-1]))


#
# fig3, axxx1 = plt.subplots()
# axxx1.plot(xs,overShootPar,colors[2]+patterns[2])
# axxx1.set_xlabel("Iteration")
# axxx1.set_ylabel("Overshoot (Global)", color=colors[2])
# axxx2 = axxx1.twinx()
# axxx2.plot(xs,throughput,colors[1]+patterns[1])
# axxx2.set_ylabel("Ideal throughput (Global)", color=colors[1])
# fig3.tight_layout()
# plt.savefig('images/global_img1.pdf', bbox_inches='tight')
#
# fig5, axx5 = plt.subplots()
# axx5.plot(xs,globalLoad, colors[2]+patterns[1])
# axx5.set_xlabel("Iteration")
# axx5.set_ylabel("load (tuples per s) [Global]")
# axx6 = axx5.twinx()
# axx6.plot(xs,NodesV,colors[3]+patterns[3])
# axx6.set_ylabel("Variation of the nodes", color=colors[3])
# fig5.tight_layout()
# plt.savefig('images/global_img3.pdf', bbox_inches='tight')

fig, (ax1, ax3) = plt.subplots(nrows=2,figsize=(8, 6))
ax1.plot(xs,globalLoad,label='Load')
ax1.set_ylabel("Load (tuples/itr)")
ax2 = ax1.twinx()
ax2.plot(xs,noeuds,colors[2]+patterns[2],label='Number of nodes')
ax2.set_ylabel("Number of nodes")
ax1.grid(linestyle=':',linewidth=0.5)
ax1.legend(bbox_to_anchor=(1, 0.3),prop={'size': 9})
ax2.legend(loc='lower right',prop={'size': 9})
ax3.plot(xs,msg,colors[3]+patterns[3],marker='.',markersize=5,label='# of messages')
ax3.grid(linestyle=':',linewidth=0.5)
ax3.set_ylabel("Number of messages")
ax3.set_xlabel("Iteration")
ax1.set_xlabel("Iteration")
ax3.legend(loc='upper right',prop={'size': 9})
ax1.set_title("(a)")
ax3.set_title("(b)")
plt.subplots_adjust(hspace=0.33)
plt.savefig('images/img4glob.pdf', bbox_inches='tight')


# fig2, axx1 = plt.subplots()
# axx1.plot(xs,overShootPar, colors[2]+patterns[1])
# axx1.set_xlabel("Iteration")
# axx1.set_ylabel("Overshoot(Global)")
# axx2 = axx1.twinx()
# axx2.plot(xs,noeuds,colors[3]+patterns[3])
# axx2.set_ylabel("Number of nodes (Global)", color=colors[3])
# fig2.tight_layout()
# plt.savefig('images/global_img6.pdf', bbox_inches='tight')

plt.legend(loc=2)
plt.grid()
fig.tight_layout()
