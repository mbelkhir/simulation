#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

colors=['b', 'g', 'r', 'c', 'm', 'y', 'k']
patterns=['','--','-.',':']

### set experience parameters

# in results-simple.dat, format is
# l,w,t

### open and clean data file
f1 = open("/home/mbelkhir/NetBeansProjects/simulator-synch-messages/metrics.dat")

data1 = f1.read()
data1 = data1.split('\n')

del data1[len(data1)-1]

### Simple - CMD ###
# plot one curve per possible length
for l in (range(200)):
    sub_data = [row for row in data1]
    xs = [sub_data_line.split('\t')[0] for sub_data_line in sub_data]
    ys = [sub_data_line.split('\t')[8] for sub_data_line in sub_data]

plt.plot(xs, ys,
        colors[3]+patterns[3],
        lw=2)



plt.legend(loc=2)
plt.xlabel("Iteration")
plt.ylabel("Number of messages in the network")
plt.grid()
plt.show()
