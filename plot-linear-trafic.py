#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import math

colors=['b', 'g', 'r', 'c', 'm', 'y', 'k']
patterns=['','--','-.',':','.']

### set experience parameters

# in results-simple.dat, format is
# l,w,t

### open and clean data file
f1 = open("metrics.dat")

data1 = f1.read()
data1 = data1.split('\n')

del data1[len(data1)-1]

### Simple - CMD ###
# plot one curve per possible length

sub_data = [row for row in data1]
xs = [sub_data_line.split('\t')[0] for sub_data_line in sub_data ]
op2 = [sub_data_line.split('\t')[6] for sub_data_line in sub_data]
nop2 = [sub_data_line.split('\t')[11] for sub_data_line in sub_data]
msg = [sub_data_line.split('\t')[8] for sub_data_line in sub_data]
# msgHist = [int(x) for x in msg]
# op2average = []
# msg2 = []
#
# for i in range(0, len(msgHist)-3, 4):
#     msg2.append(msgHist[i]+msgHist[i+1]+msgHist[i+2]+msgHist[i+3])
#
# for i in range(0, len(nop2)-3, 4):
#     op2average.append(int(float((int(nop2[i])+int(nop2[i+1])+int(nop2[i+2])+int(nop2[i+3])))/4))
#
# plt.figure(1)
# plt.bar(op2average,msg2)
# plt.xlabel("Number of nodes")
# plt.ylabel("Number of messages")
# plt.savefig('images/hist.pdf', bbox_inches='tight')

msgHist = [int(x) for x in msg]
msg2 = []
sum1 = 0
for i in range (0, 50):
    sum1 = sum1 + msgHist[i]


fig, ax1 = plt.subplots()
ax1.plot(xs,op2,colors[1]+patterns[1],label='Load')
ax1.set_xlabel("Iteration")
ax1.set_ylabel("Load (tuples/itr)")
ax2 = ax1.twinx()
ax2.plot(xs,nop2,colors[2]+patterns[2],label='# of nodes')
ax2.set_ylabel("Number of nodes")

ax3 = ax1.twinx()
ax3.plot(xs,msg,colors[3]+patterns[3],marker='.',markersize=5,label='# of messages')
ax3.set_ylabel("Number of messages")
ax3.spines["right"].set_position(("axes", 1.15))
ax1.grid(linestyle=':',linewidth=0.5)
ax1.legend(bbox_to_anchor=(0.22, 1),prop={'size': 11})
ax2.legend(bbox_to_anchor=(0.313, 0.92),prop={'size': 11})
ax3.legend(bbox_to_anchor=(0.373, 0.84),prop={'size': 11})
plt.savefig('images/traffic.pdf', bbox_inches='tight')
fig.tight_layout()
