#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import math

colors=['b', 'g', 'r', 'c', 'm', 'y', 'k']
patterns=['','--','-.',':','.']

### set experience parameters

# in results-simple.dat, format is
# l,w,t

### open and clean data file
f1 = open("metrics.dat")

data1 = f1.read()
data1 = data1.split('\n')

del data1[len(data1)-1]

### Simple - CMD ###
# plot one curve per possible length

sub_data = [row for row in data1]
xs = [sub_data_line.split('\t')[0] for sub_data_line in sub_data]
op2 = [sub_data_line.split('\t')[6] for sub_data_line in sub_data]
print(op2)
nop2 = [sub_data_line.split('\t')[11] for sub_data_line in sub_data]
msg = [sub_data_line.split('\t')[8] for sub_data_line in sub_data]
throughput = [sub_data_line.split('\t')[4] for sub_data_line in sub_data]
nodeCharge = [float(x) / float(y) for x, y in zip(op2, nop2)]
overShootAux = [float(x) - (float(y)/500) for x, y in zip(nop2, op2)]
overShootParAux = [math.ceil(float(x) - (float(y)/(500*0.7))) for x, y in zip(nop2, op2)]
overShoot = [x if x >= 0 else 0 for x in overShootAux]
overShootPar = [x if x >= 0 else 0 for x in overShootParAux]
overShootParcent = [(float(x) / float(y))*100 for x, y in zip(overShootPar, nop2)]
idealNodes = [math.ceil((float(y)/(500*0.7))) for y in op2]
needNodes = [math.ceil((float(y)/500)) for y in op2]
lostTuple = [sub_data_line.split('\t')[3] for sub_data_line in sub_data]
throughputOp2 = [sub_data_line.split('\t')[10] for sub_data_line in sub_data]


NodesV = []
NodesV.append(int(0))
for i in range(1, len(nop2)):
    NodesV.append(int(nop2[i])-int(nop2[i-1]))


UnderAndOvershoot = []
for i in range(200):
    if float(nop2[i]) / idealNodes[i] >= 1:
        UnderAndOvershoot.append(float(nop2[i]) / idealNodes[i])
    elif float(nop2[i]) / needNodes[i] >= 1:
        UnderAndOvershoot.append(1)
    else:
        UnderAndOvershoot.append(float(nop2[i]) / needNodes[i])


# fig3, axxx1 = plt.subplots()
# axxx1.plot(xs,overShootPar,colors[2]+patterns[2])
# axxx1.set_xlabel("Iteration")
# axxx1.set_ylabel("Overshoot", color=colors[2])
# axxx2 = axxx1.twinx()
# axxx2.plot(xs,throughputOp2,colors[1]+patterns[1])
# axxx2.set_ylabel("Percentage of ideal throughput", color=colors[1])
# fig3.tight_layout()
# plt.savefig('images/img1.pdf', bbox_inches='tight')

def asyp(x):
    return 1
zfig3, zaxxx1 = plt.subplots()
zaxxx1.plot(xs,UnderAndOvershoot,colors[0]+patterns[2],label='Accuracy')
zaxxx1.set_xlabel("Iteration")
zaxxx1.set_ylabel("Accuracy")
zaxxx2 = zaxxx1.twinx()
zaxxx2.plot(xs,throughputOp2,colors[1]+patterns[1],label='Throughput')
zaxxx2.set_ylabel("Percentage of ideal throughput")
zaxxx2.set_ylim(0.5,101)
zaxxx1.set_ylim(-0.1,2.6)
zaxxx1.grid(linestyle=':',linewidth=0.5)
zfig3.tight_layout()
zaxxx1.legend(bbox_to_anchor=(1, 0.18),prop={'size': 11})
zaxxx2.legend(bbox_to_anchor=(1, 0.1),prop={'size': 11})
plt.savefig('images/zimg1.pdf', bbox_inches='tight')



# plt.figure(3)
# plt.plot(xs, nodeCharge,
#         colors[4]+patterns[3],
#         lw=2)
# plt.legend(loc=2)
# plt.xlabel("Iteration")
# plt.ylabel("Load by one instance (operator 2)")
# plt.grid()
# plt.savefig('images/img2.pdf', bbox_inches='tight')
#
# plt.figure(4)
# plt.plot(xs, UnderAndOvershoot,
#         colors[1]+patterns[0],
#         lw=2)
# plt.legend(loc=2)
# plt.xlabel("Iteration")
# plt.ylabel("Accuracy")
# plt.grid()
# plt.savefig('images/img10.pdf', bbox_inches='tight')


# fig5, axx5 = plt.subplots()
# axx5.plot(xs,op2, colors[2]+patterns[1])
# axx5.set_xlabel("Iteration")
# axx5.set_ylabel("load (tuples per s) of operator 2")
# axx6 = axx5.twinx()
# axx6.plot(xs,NodesV,colors[3]+patterns[3])
# axx6.set_ylabel("Variation of the nodes", color=colors[3])
# fig5.tight_layout()
# plt.savefig('images/img3.pdf', bbox_inches='tight')


fig, (ax1, ax3) = plt.subplots(nrows=2,figsize=(8, 6))
ax1.plot(xs,op2,label='Load')
ax1.set_ylabel("Load (tuples/itr)")
ax2 = ax1.twinx()
ax2.plot(xs,nop2,colors[2]+patterns[2],label='Number of OIs')
ax2.set_ylabel("Number of OIs")
ax1.grid(linestyle=':',linewidth=0.5)
ax1.legend(loc='lower left',prop={'size': 9})
ax2.legend(loc='lower right',prop={'size': 9})
ax3.plot(xs,msg,colors[3]+patterns[3],marker='.',markersize=5,label='# of messages')
ax3.grid(linestyle=':',linewidth=0.5)
ax3.set_ylabel("Number of messages")
ax3.set_xlabel("Iteration")
ax1.set_xlabel("Iteration")
ax3.legend(loc='upper right',prop={'size': 9})
ax1.set_title("(a)")
ax3.set_title("(b)")
# idn = ax1.twinx()
# idn.plot(xs,idealNodes,colors[4]+patterns[2],label='Number of ideal OIs')
# idn.set_ylabel("Number of ideal OIs")
plt.subplots_adjust(hspace=0.33)
plt.savefig('images/img4.pdf', bbox_inches='tight')

#
# fig2, axx1 = plt.subplots()
# axx1.plot(xs,overShootParcent, colors[2]+patterns[1])
# axx1.set_xlabel("Iteration")
# axx1.set_ylabel("Overshoot per cent (operator 2)")
# axx2 = axx1.twinx()
# axx2.plot(xs,nop2,colors[3]+patterns[3])
# axx2.set_ylabel("Number of nodes (operator 2)", color=colors[3])
# fig2.tight_layout()
# plt.savefig('images/img5.pdf', bbox_inches='tight')


plt.legend(loc=2)
plt.grid()
fig.tight_layout()
